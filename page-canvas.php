<?php
/**
 * Template Name: GALS Canvas
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GALS_Mentoring_Theme
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php get_template_part('template-parts/content-header'); ?>

			<div class="canvas newsroll single margin-fix wrapper-big grid-x">

				<div class="posts cell small-12">
				<?php get_template_part('template-parts/content'); ?>
				</div>

			</div> <!-- newsroll -->


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
