<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GALS_Mentoring_Theme
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<header class="page-header latest-news-header" style="background-image: url('<?php echo get_template_directory_uri().'/images/bg/herobg.png'; ?>'">
				<div class="inner wrapper-big">
					<?php get_template_part('template-parts/breadcrumbs'); ?>
					<h1 class="page-title"><?php _e(get_the_title(get_option('page_for_posts', true))); ?></h1>
				</div>
			</header><!-- .page-header -->

			<div class="newsroll archive-news-blog margin-fix wrapper-big grid-x">
				
				<div class="posts cell large-9 medium-8 small-12">
					<div class="inner">
						<?php if ( have_posts() ) : ?>
							<?php
							/* Start the Loop */
							while ( have_posts() ) :
								the_post();
				
								/*
								* Include the Post-Type-specific template for the content.
								* If you want to override this in a child theme, then include a file
								* called content-___.php (where ___ is the Post Type name) and that will be used instead.
								*/
								get_template_part( 'template-parts/content', 'excerpt' );
				
							endwhile;
				
							get_template_part('template-parts/blocks/pagination');
				
						else :
				
							get_template_part( 'template-parts/content', 'none' );
				
						endif;
						?>
					</div>
				</div>

				<div class="sidebar-container cell large-3 medium-4 small-12">
					<?php get_sidebar(); ?>
				</div>
				
			</div>

			<?php get_template_part('template-parts/blocks/testimonials'); ?>

		

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
