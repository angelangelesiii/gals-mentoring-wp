<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GALS_Mentoring_Theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<?php 

// VARIABLES
$iconDirectory = get_template_directory_uri().'/images/icons';


?>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'galsmentoring' ); ?></a>

	<header id="main-header" class="site-header <?php
		if(is_front_page()):
			echo 'front-page';
		endif;
	?>">
	
		<nav class="site-nav wrapper-big">
			
			<div class="logo-container">
				<a href="<?php bloginfo('url') ?>">
					<img src="<?php echo get_template_directory_uri().'/images/branding/gals_full_icon.svg'; ?>" alt="<?php bloginfo('name') ?>" class="logo icon">
					<div class="logo-text">
						<img src="<?php echo get_template_directory_uri().'/images/branding/wordmark.svg'; ?>" alt="<?php bloginfo('name') ?>" class="logo wordmark">
						<blockquote class="tagline">Girls Achieving, Leading & Succeeding</blockquote>
					</div>
				</a>
			</div>

			<div class="menu-container">

				<button class="hamburger-menu hide-for-large" id="mobile-main-menu-button" aria-label="Menu">
					 <div class="bar"></div>
					 <div class="bar"></div>
					 <div class="bar"></div>
				</button>

				<?php // Call header nav menu items up to 1 depth
				wp_nav_menu( array(
					'theme_location' => 'header-menu-1',
					'menu'		=>	'header-menu-1',
					'menu_class'=>	'main-header-menu closed',
					'fallback_cb'=>	false,
					'depth'		=>	2,
					'item_spacing'=>'discard'
				) );
				?>

			</div>

		</nav>

	</header>
	
	<div id="top"></div>
	<?php if(!is_front_page()): ?>
	<div class="spacer"></div>
	<?php endif; ?>

	<div id="menu-open-overlay"></div>
	

	<div id="content" class="site-content">
