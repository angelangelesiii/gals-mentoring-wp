<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package GALS_Mentoring_Theme
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="section-404">
			
				<div class="wrapper-small inner">
				
					<div class="content-404">

						<img src="<?php echo get_template_directory_uri().'/images/svg/broken-link.svg' ?>" alt="Error" class="error-icon">

						<h2>Oops! There's nothing here.</h2>
						<p class="subtitle error-code">(Error #404)</p>
						<p class="message">The page you are looking for does not exist. It may have been deleted or you have entered a broken URL. Please contact the administrator if you need help.
						<p class="message"><a href="<?php bloginfo('url'); ?>" class="btn">Back to Home</a></p>
					
					</div>

				</div>
			
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
