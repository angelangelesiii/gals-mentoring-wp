<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GALS_Mentoring_Theme
 */

?>

	</div><!-- #content -->

	<footer id="galsfooter" class="site-footer <?php echo (is_front_page()? 'front-page' : ''); ?>">

		<div class="wrapper-big">
			<div class="footer-inner grid-x">

			<?php 
				$footerMenuCount = (get_theme_mod('footer_menu_count')? get_theme_mod('footer_menu_count'): 0);
				
				// Foundation cell size for large size
				$footerMenuLargeSize = 12;
				if($footerMenuCount == 4):
					$footerMenuLargeSize = 3;
				elseif($footerMenuCount == 3):
					$footerMenuLargeSize = 4;
				elseif($footerMenuCount == 2):
					$footerMenuLargeSize = 6;
				endif;

				$footerMenuLargeOffset = 'large-offset-';
				if($footerMenuCount == 4):
					$footerMenuLargeOffset = '';
				elseif($footerMenuCount == 3):
					$footerMenuLargeOffset = '';
				elseif($footerMenuCount <= 2):
					$footerMenuLargeOffset .= '2';
				endif;

				// Foundation cell size for medium size
				$footerMenuMediumSize = 12;
				if($footerMenuCount == 4):
					$footerMenuMediumSize = 6;
				elseif($footerMenuCount == 3):
					$footerMenuMediumSize = 4;
				elseif($footerMenuCount == 2):
					$footerMenuMediumSize = 6;
				endif;

				$footerMenuContainerLargeSize = 9;
				if($footerMenuCount == 4):
					$footerMenuContainerLargeSize = 9;
				elseif($footerMenuCount == 3):
					$footerMenuContainerLargeSize = 8;
				elseif($footerMenuCount == 2):
					$footerMenuContainerLargeSize = 5;
				else:
					$footerMenuContainerLargeSize = 4;
				endif;

				$footerMenuSubscribeLargeSize = 3;
				if($footerMenuCount == 4):
					$footerMenuSubscribeLargeSize = 12-$footerMenuContainerLargeSize;
				elseif($footerMenuCount == 3):
					$footerMenuSubscribeLargeSize = 12-$footerMenuContainerLargeSize;
				elseif($footerMenuCount == 2):
					$footerMenuSubscribeLargeSize = 3;
				else:
					$footerMenuSubscribeLargeSize = 4;
				endif;

				$footerMenuContainerMediumSize = 8;
				if($footerMenuCount == 4):
					$footerMenuContainerMediumSize = 8;
				elseif($footerMenuCount == 3):
					$footerMenuContainerMediumSize = 12;
				elseif($footerMenuCount == 2):
					$footerMenuContainerMediumSize = 8;
				else:
					$footerMenuContainerMediumSize = 6;
				endif;

				$footerSubscribeMediumOffset = 'medium-offset-';
				if($footerMenuCount == 4):
					$footerSubscribeMediumOffset = '';
				elseif($footerMenuCount == 3):
					$footerSubscribeMediumOffset .= '3';
				elseif($footerMenuCount <= 2):
					$footerSubscribeMediumOffset = '';
				endif;

				$footerMenuSubscribeMediumSize = 4;
				if($footerMenuCount == 3 || $footerMenuCount == 1) $footerMenuSubscribeMediumSize = 6;
			?>

				<?php // Menu Container ?>
				<div class="menu-container cell small-12 medium-<?php echo $footerMenuContainerMediumSize; ?> large-<?php echo $footerMenuContainerLargeSize.' '.$footerMenuLargeOffset; ?>">

					<div class="menu-row grid-x">

						<div class="cell large-<?php echo $footerMenuLargeSize?> medium-<?php echo $footerMenuMediumSize ?> small-12">
							<h3 class="menu-name"><?php echo (get_theme_mod('footer_menu_1_title')? get_theme_mod('footer_menu_1_title'): 'Footer Menu 1') ?></h3>
							<?php // Call header nav menu items up to 1 depth
							wp_nav_menu( array(
								'theme_location' => 'footer-menu-1',
								'menu'		=>	'footer-menu-1',
								'menu_class'=>	'footer-menu',
								'fallback_cb'=>	false,
								'depth'		=>	1,
								'item_spacing'=>'discard'
							) );
							?>
						</div>

						<?php if($footerMenuCount >= 2): ?>
						<div class="cell large-<?php echo $footerMenuLargeSize?> medium-<?php echo $footerMenuMediumSize ?> small-12">
							<h3 class="menu-name"><?php echo (get_theme_mod('footer_menu_2_title')? get_theme_mod('footer_menu_2_title'): 'Footer Menu 2') ?></h3>
							<?php // Call header nav menu items up to 1 depth
							wp_nav_menu( array(
								'theme_location' => 'footer-menu-2',
								'menu'		=>	'footer-menu-2',
								'menu_class'=>	'footer-menu',
								'fallback_cb'=>	false,
								'depth'		=>	1,
								'item_spacing'=>'discard'
							) );
							?>
						</div>
						<?php endif; ?>

						<?php if($footerMenuCount >= 3): ?>
						<div class="cell large-<?php echo $footerMenuLargeSize?> medium-<?php echo $footerMenuMediumSize ?> small-12">
							<h3 class="menu-name"><?php echo (get_theme_mod('footer_menu_3_title')? get_theme_mod('footer_menu_3_title'): 'Footer Menu 3') ?></h3>
							<?php // Call header nav menu items up to 1 depth
							wp_nav_menu( array(
								'theme_location' => 'footer-menu-3',
								'menu'		=>	'footer-menu-3',
								'menu_class'=>	'footer-menu',
								'fallback_cb'=>	false,
								'depth'		=>	1,
								'item_spacing'=>'discard'
							) );
							?>
						</div>
						<?php endif; ?>

						<?php if($footerMenuCount >= 4): ?>			
						<div class="cell large-<?php echo $footerMenuLargeSize?> medium-<?php echo $footerMenuMediumSize ?> small-12">
							<h3 class="menu-name"><?php echo (get_theme_mod('footer_menu_4_title')? get_theme_mod('footer_menu_4_title'): 'Footer Menu 4') ?></h3>
							<?php // Call header nav menu items up to 1 depth
							wp_nav_menu( array(
								'theme_location' => 'footer-menu-4',
								'menu'		=>	'footer-menu-4',
								'menu_class'=>	'footer-menu',
								'fallback_cb'=>	false,
								'depth'		=>	1,
								'item_spacing'=>'discard'
							) );
							?>
						</div>
						<?php endif; ?>

					</div>
	
				</div>
				
				<?php // Subscribe box container ?>
				<div class="subscribe-box cell small-12 medium-<?php echo $footerMenuSubscribeMediumSize.' '.$footerSubscribeMediumOffset; ?> large-<?php echo $footerMenuSubscribeLargeSize; ?>">
					<div class="inner">
						<h2><?php _e(get_theme_mod('footer_subscribe_message')? get_theme_mod('footer_subscribe_message') : 'Subscribe to our newsletter to stay updated!') ?> </h2>
						<?php echo (get_theme_mod('footer_subscribe_shortcode') ? do_shortcode(get_theme_mod('footer_subscribe_shortcode')) : null) ?>
					</div>
				</div>
	
			</div>
	
			<div class="colophon">
				<p>
					&copy; <?php echo date("Y"); ?> G.A.L.S. Mentoring
				</p>
				<p class="signature">Website designed and developed by <a href="https://twitter.com/angelangelesiii">Angel Angeles III</a></p>
			</div>
		</div>

	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
