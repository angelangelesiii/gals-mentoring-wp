<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GALS_Mentoring_Theme
 */

if ( ! is_active_sidebar( 'sidebar-2' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'sidebar-2' ); ?>
	
	<?php 
	if ($post->post_parent) {
        $ancestors=get_post_ancestors($post->ID);
        $root=count($ancestors)-1;
        $parent = $ancestors[$root];
	} else {
			$parent = $post->ID;
	}
	
	$children = get_pages('child_of='.$parent);
	$child_pages = array(1);
	
	foreach($children as $child) {
		array_push($child_pages,$child->ID);
	}
	$all_pages = implode(",",$child_pages);
	
	if( count( $children ) != 0 ) : ?>
	
	<div class="widget parent-menu">

		<h2 class="widget-title">
			Related Pages
		</h2>

		<ul class="sidebar-navigation">

		<?php echo wp_list_pages( 'title_li=&sort_column=menu_order&echo=0&include='.$all_pages ) ?>

		</ul>
	</div>

	<?php endif; ?>

	
</aside><!-- #secondary -->
