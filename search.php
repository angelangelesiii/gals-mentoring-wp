<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package GALS_Mentoring_Theme
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

		<header class="page-header latest-news-header" style="background-image: url('<?php echo get_template_directory_uri().'/images/bg/herobg.png'; ?>'">
				<div class="inner wrapper-big">
					<?php get_template_part('template-parts/breadcrumbs'); ?>
					<h1 class="page-title"><?php printf(esc_html__('Showing search results for: "%s"'), get_search_query()); ?></h1>
				</div>
			</header><!-- .page-header -->

			<div class="newsroll archive-news-blog margin-fix wrapper-big grid-x">
				
				<div class="posts cell large-9 medium-8 small-12">
					<div class="inner">
						<?php if ( have_posts() ) : ?>
							<?php
							/* Start the Loop */
							while ( have_posts() ) :
								the_post();
				
								/*
								* Include the Post-Type-specific template for the content.
								* If you want to override this in a child theme, then include a file
								* called content-___.php (where ___ is the Post Type name) and that will be used instead.
								*/
								get_template_part( 'template-parts/content', 'excerpt' );
				
							endwhile;
				
							get_template_part('template-parts/blocks/pagination');
				
						else :
				
							get_template_part( 'template-parts/content', 'none' );
				
						endif;
						?>
					</div>
				</div>

				<div class="sidebar-container cell large-3 medium-4 small-12">
					<?php get_sidebar(); ?>
				</div>
				
			</div>

		</main><!-- #main -->

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
