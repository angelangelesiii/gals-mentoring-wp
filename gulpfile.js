'use strict';
 
// SASS
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

sass.compiler = require('node-sass');

// JS Minify Concat
var request = require('request')
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var jsSrc = 'js/dependencies/**/*.js',
    jsPath = 'js/dependencies/',
    jsDest = 'js',
    jsConcat = [
      jsPath+'TweenMax.js',
      jsPath+'TimelineMax.js',
      jsPath+'ScrollToPlugin.js',
      jsPath+'ScrollMagic.js',
      jsPath+'animation.gsap.js',
      jsPath+'debug.addIndicators.js',
      jsPath+'slick.js', 
      jsPath+'skip-link-focus-fix.js', 
      jsPath+'main.js',
    ],
    jsConcatProd = [
      request('https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js'),
      request('https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineMax.min.js'),
      request('https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/plugins/ScrollToPlugin.min.js'),
      request('https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/jquery.ScrollMagic.min.js'),
      request('https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.min.js'),
      // jsPath+'debug.addIndicators.js',
      request('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js'), 
      jsPath+'skip-link-focus-fix.js', 
      jsPath+'main.js',
    ];

var browserList = ['last 2 versions', 'ie >= 9', 'android >= 4.4', 'ios >= 7'];
var browserList2 = 'last 2 versions';
var paths = ['node_modules/foundation-sites/scss'];
 
gulp.task('scripts', function() {
  gulp.src('./js/dependencies/map.js')
    .pipe(concat('map.min.js'))
    .pipe(gulp.dest(jsDest));

  return gulp.src(jsConcat)
    .pipe(concat('app.js'))
    .pipe(gulp.dest(jsDest));
});

gulp.task('scripts-prod', function() {
  gulp.src('./js/dependencies/map.js')
    .pipe(concat('map.min.js'))
    .pipe(gulp.dest(jsDest))
    .pipe(uglify())
    .pipe(gulp.dest(jsDest));

  return gulp.src(jsConcat)
    .pipe(concat('app.js'))
    .pipe(gulp.dest(jsDest))
    .pipe(uglify())
    .pipe(gulp.dest(jsDest));
});

gulp.task('sass-dev', function () {
  return gulp.src('./sass/**/*.scss')
    .pipe(sass.sync({
        //options
        outputStyle: 'expanded',
        includePaths: paths
    }).on('error', sass.logError))
    .pipe(autoprefixer(browserList))
    .pipe(gulp.dest('./'));
});


gulp.task('sass-prod', function () {
  
    return gulp.src('./sass/**/*.scss')
      .pipe(sass.sync({
          //options
          outputStyle: 'compressed',
          includePaths: paths
      }).on('error', sass.logError))
      .pipe(autoprefixer(browserList))
      .pipe(gulp.dest('./'));
      
      
  });
 
gulp.task('watch-dev', function () {
  gulp.watch(['./sass/**/*.scss', './js/dependencies/**/*.js'], gulp.series(['sass-dev', 'scripts']));
});

gulp.task('watch-prod', function () {
    gulp.watch(['./sass/**/*.scss', './js/dependencies/**/*.js'], gulp.series(['sass-prod', 'scripts-prod']));
  });