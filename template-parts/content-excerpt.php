<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GALS_Mentoring_Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<div class="entry-meta">
			<?php
			the_date( 'F j, Y');
			?>
		</div><!-- .entry-meta -->
		
	</header><!-- .entry-header -->

	<?php 
	if(has_post_thumbnail()): ?>
	
		<a href="<?php the_permalink(); ?>"><div class="entry-thumbnail" style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID, 'bg_medium') ?>');'"><img src="<?php echo get_the_post_thumbnail_url($post->ID, 'bg_medium') ?>" alt="<?php the_title(); ?>" class="no-show"></div></a>

	<?php
	endif;
	?>

	<div class="entry-content">
		<?php
		the_excerpt();
		?>

		<a href="<?php the_permalink(); ?>" class="read-more-btn">Read More <span class="icon-Right-7 icon"></span></a>
	</div><!-- .entry-content -->



</article><!-- #post-<?php the_ID(); ?> -->
