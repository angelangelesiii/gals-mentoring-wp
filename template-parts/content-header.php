<header class="page-header post-header">
    <div class="inner wrapper-big">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <h1 class="entry-title"><?php the_title(); ?></h1>
        <?php if(get_the_date() && is_single()): ?>
        <div class="entry-meta">
            <span class="date"><?php the_date( 'F j, Y' ) ?></span>
        </div>
        <?php endif; ?>
    </div>
</header><!-- .page-header -->