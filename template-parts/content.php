<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GALS_Mentoring_Theme
 */

?>



<div class="inner">

	<div class="content">

	<?php if(has_post_thumbnail($post->ID)): ?>
	<div class="entry-thumbnail">
		<img src="<?php echo get_the_post_thumbnail_url( $post->ID, '' ) ?>" alt="<?php the_post_thumbnail_caption(); ?>" class="featured-image">
	</div>
	<?php endif; ?>

	<?php
	while ( have_posts() ) :
		the_post();

		the_content();
	?>

	<?php

		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;

	endwhile; // End of the loop.
	?>
	</div>

</div>

