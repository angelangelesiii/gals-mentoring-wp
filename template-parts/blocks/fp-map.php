
<?php $mapPresence = true ?>


<section class="gals-map-section">

    <div class="inner grid-x">

        <div class="left cell medium-6 small-12">
            <div class="overlay"></div>
            <h2 class="section-heading">We Are <span class="em">Reaching</span> Out</h2>

        <?php if(get_theme_mod('map_repeater')): ?>
            <div id="reach-map" class="gals-map" data-locations="[<?php
                $locations = get_theme_mod('map_repeater');
                $locationsCount = count($locations);
                foreach($locations as $location):
                    preg_match('/@(\-?[0-9]+\.[0-9]+),(\-?[0-9]+\.[0-9]+)/',$location['url'],$latlng);
                    $mapLat = $latlng[1];
                    $mapLng = $latlng[2];
                    echo '{\'title\':\''.$location['title'].'\',\'lat\':'.$mapLat.',\'lng\':'.$mapLng.'}';
                    echo (++$i === $locationsCount? '' : ',');
                endforeach;
            ?>]"></div>
        <?php else: ?>
            <span class="message">There's no map data</span>
        <?php endif; ?>
        </div>

        <div class="map-message right cell medium-6 small-12">
            <div class="inner section-content">

                <ul class="messages">
                    <li class="message">
                        G.A.L.S members in <span class="em">3 states</span>
                    </li>
    
                    <li class="message">
                        Over <span class="em">200 girls</span> impacted
                    </li>

                    <li class="message">
                        <span class="em">4 chapters</span>
                    </li>
                </ul>
                
                <a href="<?php echo (get_theme_mod('map_btn_url')? get_the_permalink(get_theme_mod('map_btn_url')): 'javascript:void(0)'); ?>" class="btn btn-large cta"><?php echo (get_theme_mod('map_btn_text')? get_theme_mod('map_btn_text'): 'Call to Action Button'); ?></a>
                
            </div>
        </div>
    </div>
    

</section>