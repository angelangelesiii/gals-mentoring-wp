<section class="testimonials <?php
    if(is_front_page()):
        echo 'fp-testimonial';
    endif;
?>">

    <div class="inner wrapper-medium">
        
        <h2 class="section-heading">Testimonials</h2>

        <div class="carousel-container testimonials-container">
            
        <?php if(get_theme_mod('testimonial_repeater')): // Have testimonials?>

        <?php 
        $testimonials = get_theme_mod('testimonial_repeater');
        foreach ($testimonials as $testimonial): ?>

            <div class="testimonial">
                <div class="inner">
                    <blockquote>
                        <?php echo ($testimonial['message'] ? $testimonial['message'] : 'No message'); ?>
                        <!-- Lorem ipsum dolor sit amet consectetur, adipisicing elit. Debitis dolor, blanditiis corrupti repellendus modi, fuga accusamus culpa nulla distinctio dolore vitae, officia facilis! -->
                    </blockquote>
                    <div class="witness-photo" style="background-image: url('<?php echo ($testimonial['avatar']? wp_get_attachment_image_src($testimonial['avatar'], 'thumbnail')[0] : get_template_directory_uri().'/images/user-empty.png'); ?>'); ">
                    </div>
                    <cite class="witness-details">
                        <div class="name"><?php echo ($testimonial['name']? $testimonial['name'] : 'No Name') ?></div>
                        <div class="witness-title"><?php echo ($testimonial['title']? $testimonial['title'] : 'No Title') ?></div>
                    </cite>
                </div>
            </div>

        <?php endforeach;
        ?>


        <?php elseif(get_theme_mod('testimonial_sample_toggle')): // Have testimonials and samples are turned on ?>

            <!-- SAMPLE TESTIMONIAL -->
            <div class="testimonial">
                <div class="inner">
                    <blockquote>
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Debitis dolor, blanditiis corrupti repellendus modi, fuga accusamus culpa nulla distinctio dolore vitae, officia facilis!
                    </blockquote>
                    <div class="witness-photo" style="background-image: url('<?php echo get_template_directory_uri().'/images/1.jpg' ?>'); ">
                    </div>
                    <cite class="witness-details">
                        <div class="name">Lisa Norman</div>
                        <div class="witness-title">MIT Student</div>
                    </cite>
                </div>
            </div>

            <div class="testimonial">
                <div class="inner">
                    <blockquote>
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aut blanditiis, numquam nesciunt fuga magnam possimus impedit maxime ipsam assumenda eius! Quae obcaecati vero sit nihil quam fuga inventore, delectus ipsam eveniet repellendus eligendi, dolores et nostrum aperiam!
                    </blockquote>
                    <div class="witness-photo" style="background-image: url('<?php echo get_template_directory_uri().'/images/2.jpg' ?>'); ">
                    </div>
                    <cite class="witness-details">
                        <div class="name">Abigail Ochoa</div>
                        <div class="witness-title">Young Entrepreneur</div>
                    </cite>
                </div>
            </div>

            <div class="testimonial">
                <div class="inner">
                    <blockquote>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum quam, voluptas omnis, dolorem similique non iste amet tenetur consequatur, aspernatur maiores? Facilis ipsam ad cupiditate sunt vel consequuntur sit.
                    </blockquote>
                    <div class="witness-photo" style="background-image: url('<?php echo get_template_directory_uri().'/images/3.jpg' ?>'); ">
                    </div>
                    <cite class="witness-details">
                        <div class="name">Silvia Malone</div>
                        <div class="witness-title">Theater Pianist</div>
                    </cite>
                </div>
            </div>

        <?php endif; ?>

        </div>

    </div>

</section>