<section class="programs">

    <div class="inner wrapper-big">

        <div class="grid-x">

            <div class="medium-6 program-item">

                <?php // Get program 1 link
                    $program1Link = '';
                    if(get_theme_mod('program_1_link_url')):
                        $program1Link = (get_theme_mod( 'program_1_link_url' ));
                    elseif (get_theme_mod('program_1_page_url')):
                        $program1Link = get_the_permalink((get_theme_mod('program_1_page_url')));
                    else:
                        $program1Link = '#';
                    endif;
                ?>

                <a href="<?php echo $program1Link ?>"><div class="program-image" style="background-image: url('<?php 
                if(get_theme_mod( 'program_1_image')):
                    echo wp_get_attachment_image_src(get_theme_mod('program_1_image'), 'bg_small')[0];
                else:
                    echo get_template_directory_uri().'/images/program-1.jpg' ;
                endif;
                ?>')"></div>

                <h3>
                    <?php
                    if(get_theme_mod('program_1_header')):
                        _e(get_theme_mod('program_1_header'));
                    else:
                        _e('Program 1');
                    endif;
                    ?>
                </h3>
                </a>

                <p>
                    <?php
                    if(get_theme_mod('program_1_message')):
                        _e(get_theme_mod('program_1_message'));
                    else:
                        echo 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Recusandae, alias voluptatum? Architecto deleniti vel itaque, voluptatibus sed, laborum, eum dolorem vitae asperiores magni est odit.';
                    endif;
                    ?>
                </p>
                
            </div>

            <div class="medium-6 program-item">

                <?php // Get program 2 link
                    $program1Link = '';
                    if(get_theme_mod('program_2_link_url')):
                        $program1Link = (get_theme_mod( 'program_2_link_url' ));
                    elseif (get_theme_mod('program_2_page_url')):
                        $program1Link = get_the_permalink((get_theme_mod('program_2_page_url')));
                    else:
                        $program1Link = '#';
                    endif;
                ?>

                <a href="<?php echo $program1Link ?>"><div class="program-image" style="background-image: url('<?php 
                if(get_theme_mod( 'program_2_image')):
                    echo wp_get_attachment_image_src(get_theme_mod('program_2_image'), 'bg_small')[0];
                else:
                    echo get_template_directory_uri().'/images/program-2.jpg' ;
                endif;
                ?>')"></div>

                <h3>
                    <?php
                    if(get_theme_mod('program_2_header')):
                        _e(get_theme_mod('program_2_header'));
                    else:
                        _e('Program 2');
                    endif;
                    ?>
                </h3>
                </a>

                <p>
                    <?php
                    if(get_theme_mod('program_2_message')):
                        _e(get_theme_mod('program_2_message'));
                    else:
                        echo 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut quod nesciunt tempore rerum non culpa nemo iure corrupti, rem porro ratione placeat?';
                    endif;
                    ?>
                </p>
                
            </div>

        </div>

    </div>
</section>