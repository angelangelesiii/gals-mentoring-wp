<section class="focus">

    <div class="inner wrapper-big">

        <h2 class="section-heading">Our Key Focuses</h2>

        <div class="focus-items grid-x">
            <div class="cell large-3 medium-6 small-12">
                <img class="focus-icon" src="<?php echo get_template_directory_uri().'/images/svg/atom.svg' ?>" alt="STEM">
                <h3>S.T.E.M.</h3>
            </div>
            <div class="cell large-3 medium-6 small-12">
                <img class="focus-icon" src="<?php echo get_template_directory_uri().'/images/svg/pantheon.svg' ?>" alt="Law & Government">
                <h3>Law & Government</h3>
            </div>
            <div class="cell large-3 medium-6 small-12">
                <img class="focus-icon" src="<?php echo get_template_directory_uri().'/images/svg/cameraspeechbubble.svg' ?>" alt="Arts & Media">
                <h3>Arts & Media</h3>
            </div>
            <div class="cell large-3 medium-6 small-12">
                <img class="focus-icon" src="<?php echo get_template_directory_uri().'/images/svg/strategy-1.svg' ?>" alt="Entrepreneurship">
                <h3>Entrepreneurship</h3>
            </div>
        </div>

    </div>

</section>