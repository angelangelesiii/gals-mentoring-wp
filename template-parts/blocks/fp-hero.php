<section class="hero-section" style="background-image: url('<?php 
    if(get_theme_mod('hero_bg_image')):
        echo wp_get_attachment_image_src(get_theme_mod('hero_bg_image'), 'bg-large')[0];
    else:
        echo get_template_directory_uri().'/images/bg/herobg.png'; 
    endif;
?>');">

    <div class="section-inner" <?php 
        if(get_theme_mod('hero_overlay_transparency') != 70):
            $transparency = get_theme_mod( 'hero_overlay_transparency' )*0.01;
            echo 'style="background-image: linear-gradient(to top left, rgba(205,104,78,'.$transparency.'), rgba(158,31,97,'.$transparency.'));"';
        endif;  
    ?>>

        <div class="hero-content">
            <blockquote class="hero-text">
            <?php 
            if(get_theme_mod('hero_message')):
                _e( get_theme_mod('hero_message'));
            else:
                _e('Empowering the academic and professional advancement of girls for a brighter future. '); 
            endif;?>
            </blockquote>
            <p>
                <?php 
                    $heroBtn1Link = '';
                    if(get_theme_mod('hero_btn_1_link_url')):
                        $heroBtn1Link = (get_theme_mod( 'hero_btn_1_link_url' ));
                    elseif (get_theme_mod('hero_btn_1_page_url')):
                        $heroBtn1Link = get_the_permalink((get_theme_mod('hero_btn_1_page_url')));
                    else:
                        $heroBtn1Link = '#';
                    endif;
                ?>
                <a href="<?php echo $heroBtn1Link; ?>" class="btn btn-yellow btn-large">
                    <?php 
                    if(get_theme_mod('hero_btn_1_text')):
                        _e( get_theme_mod('hero_btn_1_text'));
                    else:
                        _e('Button 1'); 
                    endif;?>
                    <span class="icon-Right-7 btn-icon"></span>
                </a>
                <?php 
                    $heroBtn1Link = '';
                    if(get_theme_mod('hero_btn_2_link_url')):
                        $heroBtn1Link = (get_theme_mod( 'hero_btn_2_link_url' ));
                    elseif (get_theme_mod('hero_btn_2_page_url')):
                        $heroBtn1Link = get_the_permalink((get_theme_mod('hero_btn_2_page_url')));
                    else:
                        $heroBtn1Link = '#';
                    endif;
                ?>
                <a href="<?php echo $heroBtn1Link; ?>" class="btn btn-yellow btn-large">
                    <?php 
                    if(get_theme_mod('hero_btn_2_text')):
                        _e( get_theme_mod('hero_btn_2_text'));
                    else:
                        _e('Button 2'); 
                    endif;?>
                    <span class="icon-Right-7 btn-icon"></span>
                </a>
            </p>
        </div>
    
    </div>

</section>
