<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package GALS_Mentoring_Theme
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php get_template_part('template-parts/content-header'); ?>

			<div class="newsroll single margin-fix wrapper-big grid-x">

				<div class="posts cell large-9 medium-8 small-12">
				<?php get_template_part('template-parts/content'); ?>
				</div>
				
				<div class="sidebar-container cell large-3 medium-4 small-12">
				<?php get_sidebar(); ?>
				</div>

			</div> <!-- newsroll -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
