<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GALS_Mentoring_Theme
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php get_template_part('template-parts/blocks/fp-hero') ?>
		<?php get_template_part('template-parts/blocks/fp-focus') ?>
		<?php get_template_part('template-parts/blocks/fp-map') ?>
		<?php get_template_part('template-parts/blocks/fp-programs') ?>
		<?php get_template_part('template-parts/blocks/testimonials'); ?>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
