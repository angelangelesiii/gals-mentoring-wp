<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GALS_Mentoring_Theme
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php get_template_part('template-parts/content-header'); ?>

			<div class="newsroll single margin-fix wrapper-big grid-x">

				<div class="posts cell large-9 medium-8 small-12">
				<?php get_template_part('template-parts/content'); ?>
				</div>
				
				<div class="sidebar-container cell large-3 medium-4 small-12">
				<?php get_sidebar('page'); ?>
				
				</div>

			</div> <!-- newsroll -->


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
