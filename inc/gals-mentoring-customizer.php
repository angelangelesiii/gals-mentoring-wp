<?php

function galsmentoring_customize_register( $wp_customize ) {


	// THIS CUSTOMIZER REQUIRES THE KIRKI CUSTOMIZER FRAMEWORK

	if( is_plugin_active( 'kirki/kirki.php' ) ):

	Kirki::add_config( 'galsmentoring_customizer_config', array(
		'capability'    => 'edit_theme_options',
		'option_type'   => 'theme_mod',
	) );

	// Add Gals Mentoring Settings panel

	Kirki::add_panel( 'gm_settings', array(
		'title'       => esc_html__( 'GALS Mentoring Settings', 'galsmentoring' ),
		'description' => esc_html__( 'Settings for GALS Mentoring WordPress Theme', 'galsmentoring' ),
	) );

	// Hero section settings

	Kirki::add_section( 'hero_section_settings', array(
		'title'          => esc_html__( 'Hero Section', 'galsmentoring' ),
		'panel'          => 'gm_settings',
	) );
	
	// Hero section settings controls

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'        => 'image',
		'settings'    => 'hero_bg_image',
		'label'       => esc_html__( 'Image Control (URL)', 'galsmentoring' ),
		'description' => esc_html__( 'Description Here.', 'galsmentoring' ),
		'section'     => 'hero_section_settings',
		'default'     => '',
		'choices'     => [
			'save_as' => 'id',
		],
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'        => 'slider',
		'settings'    => 'hero_overlay_transparency',
		'label'       => esc_html__( 'Hero background overlay transparency', 'galsmentoring' ),
		'section'     => 'hero_section_settings',
		'default'     => 70,
		'choices'     => [
			'min'  => 0,
			'max'  => 100,
			'step' => 1,
		],
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'textarea',
		'settings' => 'hero_message',
		'label'    => esc_html__( 'Hero section banner message', 'galsmentoring' ),
		'section'  => 'hero_section_settings',
		'default'  => esc_html__( 'Empowering the academic and professional advancement of girls for a brighter future.
		', 'galsmentoring' ),
		'transport' => 'auto',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'text',
		'settings' => 'hero_btn_1_text',
		'label'    => esc_html__( 'Hero section button 1 text', 'galsmentoring' ),
		'section'  => 'hero_section_settings',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'        => 'dropdown-pages',
		'settings'    => 'hero_btn_1_page_url',
		'label'       => esc_html__( 'Hero section button 1 page', 'galsmentoring' ),
		'section'     => 'hero_section_settings',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'link',
		'settings' => 'hero_btn_1_link_url',
		'label'    => __( 'Hero section button 1 link', 'kirki' ),
		'section'  => 'hero_section_settings',
		'tooltip' => 'If set, this will override the link value of button 1. Leave blank if you don\'t want to use.',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'text',
		'settings' => 'hero_btn_2_text',
		'label'    => esc_html__( 'Hero section button 2 text', 'galsmentoring' ),
		'section'  => 'hero_section_settings',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'        => 'dropdown-pages',
		'settings'    => 'hero_btn_2_page_url',
		'label'       => esc_html__( 'Hero section button 2 page', 'galsmentoring' ),
		'section'     => 'hero_section_settings',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'link',
		'settings' => 'hero_btn_2_link_url',
		'label'    => __( 'Hero section button 12 link', 'kirki' ),
		'section'  => 'hero_section_settings',
		'tooltip' => 'If set, this will override the link value of button 2. Leave blank if you don\'t want to use.',
	] );

	endif;


	// Map section settings

	Kirki::add_section( 'map_section_settings', array(
		'title'          => esc_html__( '"Our Reach" Map', 'galsmentoring' ),
		'panel'          => 'gm_settings',
	) );

	// Map controls

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'text',
		'settings' => 'map_btn_text',
		'label'    => esc_html__( 'Call to Action Button Text', 'galsmentoring' ),
		'section'  => 'map_section_settings',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'dropdown-pages',
		'settings' => 'map_btn_url',
		'label'    => __( 'Call to Action Button Link', 'kirki' ),
		'section'  => 'map_section_settings',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'        => 'repeater',
		'label'       => esc_html__( 'Locations', 'galsmentoring' ),
		'tooltip'  	  => 'Placeholder',
		'section'     => 'map_section_settings',
		'row_label' => [
			'type'  => 'text',
			'value' => esc_html__( 'Location', 'galsmentoring' ),
		],
		'button_label' => esc_html__('Add Location', 'galsmentoring' ),
		'settings'     => 'map_repeater',
		'fields' => [
			'title' => [
				'type'        => 'text',
				'label'       => esc_html__( 'Location Title', 'galsmentoring' ),
			],
			'url' => [
				'type'        => 'url',
				'label'       => esc_html__( 'Google Maps URL', 'galsmentoring' ),
			],
		],
	] );


	// Programs section settings

	Kirki::add_section( 'programs_section_settings', array(
		'title'          => esc_html__( 'Programs Section', 'galsmentoring' ),
		'panel'          => 'gm_settings',
	) );

	// Program controls

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'        => 'image',
		'settings'    => 'program_1_image',
		'label'       => esc_html__( 'Program 1 image', 'galsmentoring' ),
		'section'     => 'programs_section_settings',
		'choices'     => [
			'save_as' => 'id',
		],
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'text',
		'settings' => 'program_1_header',
		'label'    => esc_html__( 'Program 1 header', 'galsmentoring' ),
		'section'  => 'programs_section_settings',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'textarea',
		'settings' => 'program_1_message',
		'label'    => esc_html__( 'Program 1 message', 'galsmentoring' ),
		'section'  => 'programs_section_settings',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'        => 'dropdown-pages',
		'settings'    => 'program_1_page_url',
		'label'       => esc_html__( 'Program 1 page', 'galsmentoring' ),
		'section'     => 'programs_section_settings',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'link',
		'settings' => 'program_1_link_url',
		'label'    => __( 'Program 1 link', 'kirki' ),
		'section'  => 'programs_section_settings',
		'tooltip'  => 'Use this if you want to use an external link to other website. Setting a value will override the page link selected for this program.',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'        => 'custom',
		'settings'    => 'program_hr_1',
		'section'     => 'programs_section_settings',
		'default'     => '<br/><hr/>',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'        => 'image',
		'settings'    => 'program_2_image',
		'label'       => esc_html__( 'Program 2 image', 'galsmentoring' ),
		'section'     => 'programs_section_settings',
		'choices'     => [
			'save_as' => 'id',
		],
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'text',
		'settings' => 'program_2_header',
		'label'    => esc_html__( 'Program 2 header', 'galsmentoring' ),
		'section'  => 'programs_section_settings',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'textarea',
		'settings' => 'program_2_message',
		'label'    => esc_html__( 'Program 2 message', 'galsmentoring' ),
		'section'  => 'programs_section_settings',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'        => 'dropdown-pages',
		'settings'    => 'program_2_page_url',
		'label'       => esc_html__( 'Program 2 page', 'galsmentoring' ),
		'section'     => 'programs_section_settings',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'link',
		'settings' => 'program_2_link_url',
		'label'    => __( 'Program 2 link', 'kirki' ),
		'section'  => 'programs_section_settings',
		'tooltip'  => 'Use this if you want to use an external link to other website. Setting a value will override the page link selected for this program.',
	] );


	// Testimonial section settings

	Kirki::add_section( 'testimonial_section_settings', array(
		'title'          => esc_html__( 'Testimonial Section', 'galsmentoring' ),
		'panel'          => 'gm_settings',
	) );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'        => 'repeater',
		'label'       => esc_html__( 'Testimonials', 'galsmentoring' ),
		'tooltip'  	=> 'Click the "Add Testimonial" button to add testimonials to the page. The testimonial section will show on different parts of the website. Up to 5 testimonials can exist at a time.',
		'section'     => 'testimonial_section_settings',
		'row_label' => [
			'type'  => 'text',
			'value' => esc_html__( 'Testimonial', 'galsmentoring' ),
		],
		'button_label' => esc_html__('Add Testimonial', 'galsmentoring' ),
		'settings'     => 'testimonial_repeater',
		'fields' => [
			'avatar' => [
				'type'		=> 'image',
				'label'		=> esc_html__( 'Avatar', 'galsmentoring' ),
				'choices'     => [
					'save_as' => 'id',
				],
			],
			'name' => [
				'type'        => 'text',
				'label'       => esc_html__( 'Name', 'galsmentoring' ),
			],
			'title' => [
				'type'        => 'text',
				'label'       => esc_html__( 'Title / Description', 'galsmentoring' ),
			],
			'message'  => [
				'type'        => 'textarea',
				'label'       => esc_html__( 'Testimony / Message', 'galsmentoring' ),
			],
		],
		'choices' => [
			'limit' => 5
		],
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'        => 'toggle',
		'settings'    => 'testimonial_sample_toggle',
		'label'       => esc_html__( 'Show testimonial samples', 'galsmentoring' ),
		'description' => esc_html__('Option whether to display testimonial samples when testimonial is empty.', 'galsmentoring'),
		'section'     => 'testimonial_section_settings',
		'default'     => '1',
	] );
		

	// Footer settings
	Kirki::add_section( 'footer_settings', array(
		'title'          => esc_html__( 'Site Footer', 'galsmentoring' ),
		'panel'          => 'gm_settings',
	) );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'text',
		'settings' => 'footer_subscribe_message',
		'label'    => esc_html__( 'Footer subscribe message', 'galsmentoring' ),
		'section'  => 'footer_settings',
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'text',
		'settings' => 'footer_subscribe_shortcode',
		'label'    => esc_html__( 'Footer subscribe form shortcode', 'galsmentoring' ),
		'description' => esc_html__('Paste the footer subscribe form shortcode here.', 'galsmentoring'),
		'section'  => 'footer_settings',
	] );

	Kirki::add_field( 'theme_config_id', [
		'type'        => 'select',
		'settings'    => 'footer_menu_count',
		'label'       => esc_html__( 'Footer menu count', 'galsmentoring' ),
		'section'     => 'footer_settings',
		'description' => esc_html__('Select how many menus should appear in the website footer.', 'galsmentoring'),
		'default'     => '4',
		'placeholder' => esc_html__( 'Select one...', 'galsmentoring' ),
		'multiple'    => 1,
		'choices'     => [
			'1' => esc_html__( '1', 'galsmentoring' ),
			'2' => esc_html__( '2', 'galsmentoring' ),
			'3' => esc_html__( '3', 'galsmentoring' ),
			'4' => esc_html__( '4', 'galsmentoring' ),
		],
	] );
	
	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'text',
		'settings' => 'footer_menu_1_title',
		'label'    => esc_html__( 'Footer Menu 1 Title', 'galsmentoring' ),
		'section'  => 'footer_settings',
		'active_callback'  => [
			[
				'setting'  => 'footer_menu_count',
				'operator' => '>=',
				'value'    => 1,
			]
		]
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'text',
		'settings' => 'footer_menu_2_title',
		'label'    => esc_html__( 'Footer Menu 2 Title', 'galsmentoring' ),
		'section'  => 'footer_settings',
		'active_callback'  => [
			[
				'setting'  => 'footer_menu_count',
				'operator' => '>=',
				'value'    => 2,
			]
		]
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'text',
		'settings' => 'footer_menu_3_title',
		'label'    => esc_html__( 'Footer Menu 3 Title', 'galsmentoring' ),
		'section'  => 'footer_settings',
		'active_callback'  => [
			[
				'setting'  => 'footer_menu_count',
				'operator' => '>=',
				'value'    => 3,
			]
		]
	] );

	Kirki::add_field( 'galsmentoring_customizer_config', [
		'type'     => 'text',
		'settings' => 'footer_menu_4_title',
		'label'    => esc_html__( 'Footer Menu 4 Title', 'galsmentoring' ),
		'section'  => 'footer_settings',
		'active_callback'  => [
			[
				'setting'  => 'footer_menu_count',
				'operator' => '>=',
				'value'    => 4,
			]
		]
	] );


}
add_action( 'customize_register', 'galsmentoring_customize_register' );