# GALS Mentoring Wordpress Theme

This is the official WordPress theme for GALS Mentoring. 

---

## Usage
Download the whole folder and place into your WordPress themes folder then go to your *WordPress admin dashboard > Appearance > Themes* and then activate GALS Mentoring theme.

---


<http://galsmentoring.com>

[Angel Angeles's Twitter](https://twitter.com/angelangelesiii)