
// JQuery WordPress workaround
// (function($) {})( jQuery ); 

$(document).ready(function (e) {

    // Slick front page testimonials
    $('.testimonials-container').slick({
        autoplay: true,
        autoplaySpeed: 10000,
        speed: 1000,
        prevArrow: '<button type="button" class="slick-btn slick-prev" aria-label="previous testimonial"><span class="icon-Left-7"></button>',
        nextArrow: '<button type="button" class="slick-btn slick-next" aria-label="next testimonial"><span class="icon-Right-7"></button>',
    });

    // Initialize a ScrollMagic controller. This will take care
    // of all ScrollMagic scenes.
    var controller = new ScrollMagic.Controller();

    // Header ScrollMagic Scene
    // For main header to change size after scrolling from top

    function headerClassAdd() {
        $('#main-header').addClass('small');
    }

    function headerClassRemove() {
        $('#main-header').removeClass('small');
    }

    var headerChange = new ScrollMagic.Scene({
        triggerElement: '#top',
        triggerHook: 0,
        offset: 110,
    })
    .on('enter', headerClassAdd)
    .on('leave', headerClassRemove)
    // .addIndicators()
    .addTo(controller);
    
    
    // Mobile menu controls 

    var mainHeaderMenu = $('#menu-main-header-menu'),
        hamburgerMenu = $('#mobile-main-menu-button'),
        logoContainer = $('#main-header .logo-container'),
        menuOverlay = $('#menu-open-overlay');

    $('#mobile-main-menu-button').on('click', function() {
        if(mainHeaderMenu.hasClass('closed')) {
            mainHeaderMenu.removeClass('closed');
            hamburgerMenu.addClass('x-button');
            $('body').addClass('dont-move');
            logoContainer.addClass('sink');
            menuOverlay.addClass('overlay');
        } else {
            mainHeaderMenu.addClass('closed');
            hamburgerMenu.removeClass('x-button');
            $('body').removeClass('dont-move');
            logoContainer.removeClass('sink');
            menuOverlay.removeClass('overlay');
        }
    });
    

});