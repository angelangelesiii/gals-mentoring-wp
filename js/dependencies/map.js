// gals map

var mapStyle = [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#dec7a4" //#ffcadc
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": "100"
            }
        ]
    }
]
// end var mapStyle
var locationSrc = $('#reach-map').data('locations');
locationSrc = locationSrc.replace(/'/g, '"');
var locations = JSON.parse(locationSrc);
console.log(locations);



// Initialize and add the map
// The location of Uluru
var center = {lat: 32.5931636, lng: -91.7361816};
// The map, centered at Uluru
var map = new google.maps.Map(
    document.getElementById('reach-map'), {
        zoom: 6.5, 
        center: center,
        styles: mapStyle,
        disableDefaultUI: true,
        gestureHandling: 'none',
        zoomControl: false,
        draggableCursor: 'default'
    });

// The marker, positioned at Uluru
// var marker = new google.maps.Marker({
//     position: center,
//     map: map,
//     });

var bounds = new google.maps.LatLngBounds();

var icon = {
    url: '/wp-content/themes/galsmentoring/images/svg/map-pin.svg', // url
    scaledSize: new google.maps.Size(75, 75), // scaled size
};

for(i = 0; i<locations.length; i++) {
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
        title: locations[i].title,
        map: map,
        icon: icon,
        });

    bounds.extend(marker.position);
}


map.fitBounds(bounds);